Condition Namespace
===================

The Condition space defines a number functions to test for certain states when doing conditional testing.
It currently cannot be extended via the extension API.

Functions
---------

.. autofunction:: autest.conditions.isplatform.IsPlatform
.. autofunction:: autest.conditions.isplatform.IsNotPlatform
.. autofunction:: autest.conditions.process.RunCommand
.. autofunction:: autest.conditions.process.CheckOutput
.. autofunction:: autest.conditions.process.EnsureVersion
.. autofunction:: autest.conditions.process.HasProgram
.. autofunction:: autest.conditions.process.IsElevated
.. autofunction:: autest.conditions.process.HasPythonPackage
.. autofunction:: autest.conditions.regkey.HasRegKey
