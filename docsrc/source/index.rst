
##################################
Welcome to AuTest's documentation!
##################################

.. automodule:: autest


.. toctree::
    :maxdepth: 2

    overview/introduction

    basics/general_design
    basics/core_items

    API/main

    usage/cli

    changelog
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
